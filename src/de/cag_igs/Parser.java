/*
Copyright (c) 2016 Markus Fischer

This software is provided 'as-is', without any express or implied warranty. In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

*/
package de.cag_igs;

import java.net.Inet4Address;

public class Parser {
    public static boolean isComment(String line) {
        return !line.isEmpty() && line.charAt(0) == ';';
    }

    public static boolean isMemoryModifier(String line) {
        return !line.isEmpty() && line.matches("\\$\\d?\\d?\\d?\\ \\d+");
    }

    public static boolean isValidCommand(String line) {
        if (!line.isEmpty()) {
            line = line.toUpperCase();
            if (line.matches("(TAKE{1}|SAVE{1}|ADD{1}|SUB{1}|TST{1}|INC{1}|DEC{1}|NULL{1})\\ \\$\\d{1,3}")) {
                return true;
            } else {
                if (line.matches("JMP{1}\\ \\w*")) {
                    return true;
                } else {
                    return line.startsWith("HLT");
                }
            }
        } else {
            return false;
        }
    }

    public static boolean interactsWithValidMemoryAddress(String line) {
        if (!line.isEmpty()) {
            if (line.matches("\\w+\\ \\$\\d{1,3}")) {
                int index = line.indexOf('$');
                String numberstring = line.substring(index + 1, line.length());
                int number = Integer.parseInt(numberstring);
                return (number >= 0 && number <= 99);
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static boolean isJumpMarker(String line) {
        if (!line.isEmpty()) {
            line = line.toUpperCase();
            return line.matches("[A-Z]+:{1}");
        } else {
            return false;
        }
    }

    public static boolean isJump(String line) {
        return !line.isEmpty() && line.matches("JMP{1}\\ \\w*");
    }

    public static boolean isHlt(String line) {
        return !line.isEmpty() && line.matches("(HLT{1}).*");
    }

    public static String getJumpLocation(String line) {
        if (isJump(line)) {
            String part = line.substring(4, line.length());
            part = part.toUpperCase();
            if (part.matches("[A-Z]+")) {
                return part;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public static String getJumpName(String line) {
        if (isJumpMarker(line)) {
            String part = line.substring(0, line.length() - 1);
            part = part.toUpperCase();
            return part;
        } else {
            return null;
        }
    }

    public static String translateToOpcode(String line) {
        if (isValidCommand(line)) {
            String[] cmd = line.split("\\ ");
            String opcode = null;
            cmd[0] = cmd[0].toUpperCase();
            switch (cmd[0]) {
                case "TAKE":
                    opcode = "1";
                    if (cmd[1].substring(1).length() == 2) {
                        opcode += "0";
                        opcode += cmd[1].substring(1);
                    } else if (cmd[1].substring(1).length() == 1) {
                        opcode += "00";
                        opcode += cmd[1].substring(1);
                    } else {
                        opcode += cmd[1].substring(1);
                    }
                    break;
                case "SAVE":
                    opcode = "4";
                    if (cmd[1].substring(1).length() == 2) {
                        opcode += "0";
                        opcode += cmd[1].substring(1);
                    } else if (cmd[1].substring(1).length() == 1) {
                        opcode += "00";
                        opcode += cmd[1].substring(1);
                    } else {
                        opcode += cmd[1].substring(1);
                    }
                    break;
                case "ADD":
                    opcode = "2";
                    if (cmd[1].substring(1).length() == 2) {
                        opcode += "0";
                        opcode += cmd[1].substring(1);
                    } else if (cmd[1].substring(1).length() == 1) {
                        opcode += "00";
                        opcode += cmd[1].substring(1);
                    } else {
                        opcode += cmd[1].substring(1);
                    }
                    break;
                case "SUB":
                    opcode = "3";
                    if (cmd[1].substring(1).length() == 2) {
                        opcode += "0";
                        opcode += cmd[1].substring(1);
                    } else if (cmd[1].substring(1).length() == 1) {
                        opcode += "00";
                        opcode += cmd[1].substring(1);
                    } else {
                        opcode += cmd[1].substring(1);
                    }
                    break;
                case "TST":
                    opcode = "6";
                    if (cmd[1].substring(1).length() == 2) {
                        opcode += "0";
                        opcode += cmd[1].substring(1);
                    } else if (cmd[1].substring(1).length() == 1) {
                        opcode += "00";
                        opcode += cmd[1].substring(1);
                    } else {
                        opcode += cmd[1].substring(1);
                    }
                    break;
                case "JMP":
                    opcode = "5000";
                    break;
                case "INC":
                    opcode = "7";
                    if (cmd[1].substring(1).length() == 2) {
                        opcode += "0";
                        opcode += cmd[1].substring(1);
                    } else if (cmd[1].substring(1).length() == 1) {
                        opcode += "00";
                        opcode += cmd[1].substring(1);
                    } else {
                        opcode += cmd[1].substring(1);
                    }
                    break;
                case "DEC":
                    opcode = "8";
                    if (cmd[1].substring(1).length() == 2) {
                        opcode += "0";
                        opcode += cmd[1].substring(1);
                    } else if (cmd[1].substring(1).length() == 1) {
                        opcode += "00";
                        opcode += cmd[1].substring(1);
                    } else {
                        opcode += cmd[1].substring(1);
                    }
                    break;
                case "NULL":
                    opcode = "9";
                    if (cmd[1].substring(1).length() == 2) {
                        opcode += "0";
                        opcode += cmd[1].substring(1);
                    } else if (cmd[1].substring(1).length() == 1) {
                        opcode += "00";
                        opcode += cmd[1].substring(1);
                    } else {
                        opcode += cmd[1].substring(1);
                    }
                    break;
                case "HLT":
                    opcode = "10000";
                    break;
                default:
                    return null;
            }
            int i = Integer.parseInt(opcode);
            ++i;
            return Integer.toString(i);
        } else {
            return null;
        }
    }

    public static int getMemoryModifierAddress(String line) {
        if (isMemoryModifier(line)) {
            String[] parts = line.split("\\ ");
            String addresspart = parts[0].substring(1);
            int val = Integer.parseInt(addresspart);
            if (val < 0 || val > 99) {
                return -1;
            } else {
                return val + 1;
            }
        } else {
            return -1;
        }
    }

    public static int getMemoryModiferValue(String line) {
        if (isMemoryModifier(line)) {
            String[] parts = line.split("\\ ");
            String valuepart = parts[1];
            int val = Integer.parseInt(valuepart);
            if (val < 0 || val > 99999) {
                return -1;
            } else {
                return val;
            }
        } else {
            return -1;
        }
    }
}
