/*
Copyright (c) 2016 Markus Fischer

This software is provided 'as-is', without any express or implied warranty. In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

*/
package de.cag_igs;

import java.io.*;
import java.nio.charset.Charset;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        if (args.length < 2) {
            System.out.println("Wrong usage!");
            System.out.println("Usage: jasm [args] <input> <output>");
            return;
        }
        if (args[1].contains(".ram") || args[0].contains(".asm")) {
            System.out.println("Bitte keine Dateiendungen angeben!");
            return;
        }
        try {
            InputStream input = new FileInputStream(args[0] + ".asm");
            InputStreamReader inputStreamReader = new InputStreamReader(input, Charset.forName("UTF-8"));
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            BufferedWriter outputFile = new BufferedWriter(new FileWriter(args[1] + ".obj"));
            outputFile.flush();
            outputFile.write("5151");
            outputFile.newLine();
            int outputFileNumber = 1;
            for (; outputFileNumber < 151; ++outputFileNumber) {
                outputFile.write("000");
                outputFile.newLine();
            }
            String line;
            int linenumber = 0;
            Map<Integer, String> jumpTo = new HashMap<>(); //JMP Address + Label name
            Map<String, Integer> jumpLabels = new HashMap<>(); //label + jump address
            ArrayList<MemoryModiferValue> memoryModifiers = new ArrayList<>();
            while ((line = bufferedReader.readLine()) != null) {
                ++linenumber;
                if (!Parser.isComment(line)) {
                    if (Parser.isValidCommand(line) || Parser.isJumpMarker(line) || Parser.isMemoryModifier(line)) {
                        if (Parser.isHlt(line)) {
                            //TODO add warnings
                            outputFile.write(Parser.translateToOpcode(line));
                            outputFile.newLine();
                            ++outputFileNumber;
                            continue;
                        } else if (Parser.isJumpMarker(line)) {
                            jumpLabels.put(Parser.getJumpName(line), outputFileNumber + 1);
                        } else if (Parser.isJump(line)) {
                            outputFile.write(Parser.translateToOpcode(line));
                            outputFile.newLine();
                            jumpTo.put(++outputFileNumber, Parser.getJumpLocation(line));
                        } else if (Parser.isMemoryModifier(line)) {
                            int adr, val;
                            adr = Parser.getMemoryModifierAddress(line);
                            val = Parser.getMemoryModiferValue(line);
                            if (adr == -1 || val == -1) {
                                System.out.println("MemoryModifer address or value is wrong! Line " + linenumber);
                                return;
                            }
                            memoryModifiers.add(new MemoryModiferValue(adr, val));
                        } else if (Parser.interactsWithValidMemoryAddress(line)) {
                            outputFile.write(Parser.translateToOpcode(line));
                            outputFile.newLine();
                            ++outputFileNumber;
                        } else {
                            System.out.println("No interaction with valid memory address in line " + linenumber);
                            return;
                        }

                    } else {
                        System.out.println("Unknown command in line " + linenumber);
                        return;
                    }
                }

            }
            while (++outputFileNumber <= 1000) {
                outputFile.write("000");
                outputFile.newLine();
            }
            outputFile.close();
            bufferedReader.close();
            inputStreamReader.close();
            input.close();

            //TODO find a faster way to write jumps to the ram file

            //überprüfe, ob alle Sprunganweisung zu einer gültigen Anweisung springen können
            for (int i = 0; i < jumpTo.size(); ++i) {
                if (!jumpLabels.containsKey(jumpTo.values().toArray()[i])) {
                    System.out.println("Label: " + jumpTo.values().toArray()[i] + " is unknown!");
                    return;
                }
            }
            input = new FileInputStream(args[1] + ".obj");
            inputStreamReader = new InputStreamReader(input, Charset.forName("UTF-8"));
            bufferedReader = new BufferedReader(inputStreamReader);
            int mvc = 0;
            String tempFileName = args[1] + ".ram";
            BufferedWriter tempFile = new BufferedWriter(new FileWriter(tempFileName));
            tempFile.flush();
            String temp;
            int ln = 0;
            //memory Modifiers nach Zugriffsadresse sortieren
            memoryModifiers.sort(new Comparator<MemoryModiferValue>() {
                @Override
                public int compare(MemoryModiferValue o1, MemoryModiferValue o2) {
                    return Integer.compare(o1.getAddress(), o2.getAddress());
                }
            });
            while ((temp = bufferedReader.readLine()) != null) {
                if (ln++ == 0) {
                    tempFile.write(temp);
                    tempFile.newLine();
                    continue; //Überspring erste Sprunganweisung
                }
                if (mvc <= memoryModifiers.size() - 1) {
                    //Eine Zeile weiter vorne (erste Zeile gleich Sprung zum Programm)
                    if (ln == memoryModifiers.get(mvc).getAddress() + 1) {
                        temp = Integer.toString(memoryModifiers.get(mvc).getValue());
                        ++mvc;
                    }
                }
                if (jumpTo.containsKey(ln)) {
                    temp = "5" + (jumpLabels.get(jumpTo.get(ln)) - 1);
                }
                tempFile.write(temp);
                tempFile.newLine();
            }
            tempFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
