/*
Copyright (c) 2016 Markus Fischer

This software is provided 'as-is', without any express or implied warranty. In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

*/
package de.cag_igs;

import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class ParserTest {

    @Test
    public void testIsComment() throws Exception {
        String comment = ";some comment";
        String noComment = "TAKE nocomment";
        assertTrue("Comment: ", Parser.isComment(comment));
        assertFalse("No Comment: ", Parser.isComment(noComment));
    }

    @Test
    public void testIsMemoryModifier() throws Exception {
        String memorymodifier = "$005 55555";
        String looksLikeMemoryModifier = "$353 abc";
        String noMemoryModifier = "something";
        String noMemoryModifier2 = "234 325";
        assertTrue("Memorymodifer: ", Parser.isMemoryModifier(memorymodifier));
        assertFalse("Looks like a memorymodifer: ", Parser.isMemoryModifier(looksLikeMemoryModifier));
        assertFalse("no memorymodifer: ", Parser.isMemoryModifier(noMemoryModifier));
        assertFalse("no memorymodifer 2: ", Parser.isMemoryModifier(noMemoryModifier2));
    }

    @Test
    public void testIsValidCommand() throws Exception {
        String[] validstrings = {"TAKE $042", "SAVE $042", "ADD $042", "SUB $042", "TST $042", "JMP string", "INC $042",
                "DEC $042", "NULL $042", "HLT", "HLT $042", "HLT str"};
        String[] notValidString = {"TAKE", "SAVE", "ADD", "SUB", "TST", "JMP", "INC", "DEC", "NULL", "JMP $042",
                "TAKE 42", "SAVE 42", "ADD 42", "SUB 042", "TST 042", "INC 042",
                "DEC 042", "NULL 042", "TAKE str", "SAVE str", "ADD str", "SUB str", "TST str",
                "INC str", "DEC str", "NULL str", ";str", "SOME $042"};
        for (String str : validstrings) {
            assertTrue("Valid (" + str + ")", Parser.isValidCommand(str));
        }

        for (String str : notValidString) {
            assertFalse("Not valid (" + str + ")", Parser.isValidCommand(str));
        }
    }

    @Test
    public void testInteractsWithValidMemoryAddress() throws Exception {
        ArrayList<String> validStrings = new ArrayList<>();
        validStrings.add("TAKE $042");
        for (int i = 0; i < 100; ++i) {
            validStrings.add("TAKE " + "$" + Integer.toString(i));
        }

        ArrayList<String> unvalidStrings = new ArrayList<>();
        unvalidStrings.add("SAVE 042");
        for (int i = 0; i < 100; ++i) {
            unvalidStrings.add("SAVE " + Integer.toString(i));
        }
        for (int i = 100; i < 1000; ++i) {
            unvalidStrings.add("SAVE " + "$" + Integer.toString(i));
        }

        for (String str : validStrings) {
            assertTrue("Valid (" + str + ")", Parser.interactsWithValidMemoryAddress(str));
        }

        for (String str : unvalidStrings) {
            assertFalse("Unvalid (" + str + ")", Parser.interactsWithValidMemoryAddress(str));
        }
    }

    @Test
    public void testIsJumpMarker() throws Exception {
        String[] validJumpMarkers = {"jump:", "JUMP:", "jUmP:"};
        String[] unvalidJumpMarkers = {"jump", "j42p", "f00:"};
        for (String str : validJumpMarkers) {
            assertTrue("Valid (" + str + ")", Parser.isJumpMarker(str));
        }

        for (String str : unvalidJumpMarkers) {
            assertFalse("Not valid (" + str + ")", Parser.isJumpMarker(str));
        }
    }

    @Test
    public void testIsJump() throws Exception {
        String jump = "JMP str";
        String nojump = "JUMP";
        assertTrue("isJump", Parser.isJump(jump));
        assertFalse("isNoJump", Parser.isJump(nojump));
    }

    @Test
    public void testIsHlt() throws Exception {
        String hlt = "HLT str";
        String noHlt = "HALT";
        assertTrue("isHLT", Parser.isHlt(hlt));
        assertFalse("isNoHLT", Parser.isHlt(noHlt));
    }

    @Test
    public void testGetJumpLocation() throws Exception {
        String[] testStrings = {"JMP blafasel", "JMP BLAblub", "HLT blub", "JMP doof:"};
        String[] testResults = {"BLAFASEL", "BLABLUB", null, null};
        for (int i = 0; i < testStrings.length; ++i) {
            assertEquals("Teststring: " + testStrings[i], testResults[i], Parser.getJumpLocation(testStrings[i]));
        }
    }

    @Test
    public void testGetJumpName() throws Exception {
        String[] testStrings = {"blafasel:", "BLAblub:", "blub", "d00f:"};
        String[] testResults = {"BLAFASEL", "BLABLUB", null, null};
        for (int i = 0; i < testStrings.length; ++i) {
            assertEquals("Teststring: " + testStrings[i], testResults[i], Parser.getJumpName(testStrings[i]));
        }
    }

    @Test
    public void testTranslateToOpcode() throws Exception {
        String[] testStrings = {"TAKE $042", "SAVE $042", "ADD $042", "SUB $042", "TST $042", "JMP jump", "INC $042",
                "DEC $042", "NULL $042", "HLT $042", "NULL 42"};
        String[] testResults = {"1043", "4043", "2043", "3043", "6043", "5001", "7043", "8043", "9043", "10001", null};
        for (int i = 0; i < testStrings.length; ++i) {
            assertEquals("Teststring: " + testStrings[i], testResults[i], Parser.translateToOpcode(testStrings[i]));
        }
    }

    @Test
    public void testGetMemoryModifierAddress() throws Exception {
        String validValue = "$42 42";
        String unvalidValue = "$420 42";
        assertEquals("Valid address!", 43, Parser.getMemoryModifierAddress(validValue));
        assertEquals("unvalid address!", -1, Parser.getMemoryModifierAddress(unvalidValue));
    }

    @Test
    public void testGetMemoryModifierValue() throws Exception {
        String validValue = "$42 42";
        String unvalidValue = "$42 42424242";
        String negativValue = "$42 -42";
        assertEquals("Valid address!", 42, Parser.getMemoryModiferValue(validValue));
        assertEquals("unvalid address!", -1, Parser.getMemoryModiferValue(unvalidValue));
        assertEquals("negativ value!", -1, Parser.getMemoryModiferValue(negativValue));
    }
}