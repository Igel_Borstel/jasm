
Branchs
* master - stellt die jeweils aktuellste Version dar
* stable-x.x - stellt eine jeweils veraltete Version dar (ehemals master)
* bugfixing - geklonter masterbranch, bugfixing findet hier statt
* beta - stellt jeweils eine neue Betaversion dar (semistable)
* devil - Entwicklungszweig wo alle Features zusammenlaufen. (häufige Änderung)
* beliebiger anderer Name - Entwicklungszweig für neue Features

contribution:
Wenn neues Feature gewünscht wird bitte im Issuetracker eintragen bzw. Projekt 
forken, neuer Branch (siehe obrige Richtlinie) und merge request stellen

Der masterbranch ist mit der ersten offiziellen Version geschützt und wird 
jeweils nur durch den bugfixing bzw. den beta branch erweitert. Wenn es einmal 
eine 1.0 Version gibt muss immer aus dem masterbranch eine stabile Version 
gebaut werden können.

Steht eine neue Releaseversion bevor wird der masterbranch in einen 
entsprechenden stable-x.x branch geklont. Dadurch können zu jedem Zeitpunkt alle
offizielle alten Versionen gebaut werden.
Im Anschluss wird der (hoffentlich) bugfreie mit dem betabranch zusammengeführt.
Außerdem wird der bugfixingbranch neu aus dem master erstellt.

Tritt ein Bug auf, der noch im aktuellen majorrelease verarbeitet werden soll
findet dies im Bugfixing-Zweig statt. Ansonsten wird dies im devilzweig gemacht.
Ist eine neue Version im Bugfixingzweig lauffähig wird diese mit dem master und
mit dem devil branch vereinigt.

Der Betabranch stellt ähnlich wie der masterbranch die jeweils aktuelle Beta dar.
Alphaversion stammen aus dem devil Zweig. Jede Person, die gerade nicht am 
bugfixing arbeit tätigt die Entwicklung an neuen Features in einem Zweig mit
dem jeweiligen Namen.